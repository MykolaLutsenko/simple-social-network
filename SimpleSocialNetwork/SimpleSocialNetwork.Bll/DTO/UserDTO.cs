﻿using System.Collections.Generic;

namespace SimpleSocialNetwork.Bll.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public virtual UserProfileDTO UserProfile { get; set; }
        public virtual ICollection<FriendRelationDTO> Friends { get; set; }
        public virtual ICollection<FriendRequestDTO> Requests { get; set; }
        public virtual ICollection<PostDTO> Posts { get; set; }
        public virtual ICollection<TalkDTO> Talks { get; set; }
        public UserDTO()
        {
            Requests = new List<FriendRequestDTO>();
            Friends = new List<FriendRelationDTO>();
            Posts = new List<PostDTO>();
            Talks = new List<TalkDTO>();
        }
    }
}