﻿using System.Collections.Generic;

namespace SimpleSocialNetwork.Bll.DTO
{
    public class TalkDTO
    {
        public int Id { get; set; }
        public virtual UserDTO FromWho { get; set; }
        public virtual UserDTO ToWho { get; set; }
        public virtual ICollection<MessageDTO> Messages { get; set; }
        public bool HasNew { get; set; }

        public TalkDTO()
        {
            Messages = new List<MessageDTO>();
        }
    }
}