﻿namespace SimpleSocialNetwork.Bll.DTO
{
    public class UserProfileDTO
    {
        public int Id { get; set; }
        public Gender Gender { get; set; }
        public string Town { get; set; }
        public string Country { get; set; }
        public string UserAvatar { get; set; }
        public int? UserId { get; set; }
        public virtual UserDTO User { get; set; }
    }
}