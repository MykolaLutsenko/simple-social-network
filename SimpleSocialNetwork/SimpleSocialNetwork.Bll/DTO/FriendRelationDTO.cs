﻿namespace SimpleSocialNetwork.Bll.DTO
{
    public class FriendRelationDTO
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }
        public virtual UserDTO Parent { get; set; }

        public int? ChildId { get; set; }
        public virtual UserDTO Child { get; set; }
    }
}