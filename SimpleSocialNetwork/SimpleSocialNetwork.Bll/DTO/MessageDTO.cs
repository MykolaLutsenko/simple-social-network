﻿using System;
using System.Collections.Generic;

namespace SimpleSocialNetwork.Bll.DTO
{
    public class MessageDTO
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Text { get; set; }
        public virtual TalkDTO Talk { get; set; }
        public virtual UserDTO User { get; set; }
        public bool IsRead { get; set; }
    }
}