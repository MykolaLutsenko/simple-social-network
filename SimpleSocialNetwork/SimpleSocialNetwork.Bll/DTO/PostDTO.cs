﻿using System;

namespace SimpleSocialNetwork.Bll.DTO
{
    public class PostDTO
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string PostText { get; set; }
        public int? UserId { get; set; }
        public virtual UserDTO User { get; set; }
    }
}