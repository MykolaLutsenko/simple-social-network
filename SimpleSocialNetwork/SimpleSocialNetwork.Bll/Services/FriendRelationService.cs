﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SimpleSocialNetwork.Bll.DTO;
using SimpleSocialNetwork.Bll.Infrastructure;
using SimpleSocialNetwork.Bll.Interfaces;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Bll.Services
{
    public class FriendRelationService : IFriendRelationService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _db;

        public FriendRelationService(IUnitOfWork uow)
        {
            _db = uow;
            _mapper = AutoMapperConfigBll.Mapper;
        }

        public void Add(int parentId, int childId)
        {
            var result = new FriendRelation()
            {
                Parent = _db.Users.Find(x=>x.Id == parentId).First(),
                ParentId = parentId,
                Child = _db.Users.Find(x=>x.Id == childId).First(),
                ChildId = childId
            };

            _db.FriendRelations.Create(result);
            _db.Save();
        }

        public FriendRelationDTO Get(int id)
        {
            var result = _db.FriendRelations.GetAll().First(x => x.Id == id);
            if (result == null)
                throw new NullReferenceException($"User with this id: {id} not ound.");
            return _mapper.Map<FriendRelation,FriendRelationDTO>(result);
        
        }

        public IList<FriendRelationDTO> GetAll()
        {
            return _mapper.Map<IList<FriendRelation>, IList<FriendRelationDTO>>(_db.FriendRelations.GetAll().ToList());
        }

        public void Remove(int id)
        {
            var result = _db.FriendRelations.Find(fr => fr.Id == id);
            if(result == null)
                throw new NullReferenceException($"FriendRelation with this id: {id} not found.");
            _db.FriendRelations.Delete(id);
            _db.Save();
        }

        public IList<FriendRelationDTO> GetAllWithDetails()
        {
            return _mapper.Map<IList<FriendRelation>,IList<FriendRelationDTO>>(_db.FriendRelations.ReturnAllWithDetails());
        }

        public void Update(FriendRelationDTO item)
        {
            var temp = _db.FriendRelations.Find(fr => fr.Id == item.Id).First();

            var updateItem = _mapper.Map<FriendRelationDTO, FriendRelation>(item);

            var changed = FieldUpdater(updateItem, temp);

            _db.FriendRelations.Update(changed);
            _db.Save();
        }

        private FriendRelation FieldUpdater(FriendRelation changes, FriendRelation changed)
        {
            changed.ParentId = changes.ParentId;
            changed.Parent = changes.Parent;
            changed.ChildId = changes.ChildId;
            changed.Child = changes.Child;
            
            return changed;
        }
    }
}