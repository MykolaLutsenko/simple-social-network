﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SimpleSocialNetwork.Bll.DTO;
using SimpleSocialNetwork.Bll.Infrastructure;
using SimpleSocialNetwork.Bll.Interfaces;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Bll.Services
{
    public class TalkService : ITalkService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _db;

        public TalkService(IUnitOfWork uow)
        {
            _db = uow;
            _mapper = AutoMapperConfigBll.Mapper;
        }

        public void Add(TalkDTO item)
        {
            if(item == null)
                throw new ValidationException("Item can not be null",$"{nameof(item)}");
            var temp = _mapper.Map<TalkDTO, Talk>(item);
            _db.Talks.Create(temp);
        }

        public TalkDTO Get(int id)
        {
            var result = _db.Talks.GetAll().First(x => x.Id == id);
            if(result == null)
                throw new ValidationException("Not found item with this id",$"{nameof(id)}");
            return _mapper.Map<Talk, TalkDTO>(result);
        }

        public IList<TalkDTO> GetAll()
        {
            var result = _db.Talks.ReturnAllWithDetails();
            return _mapper.Map<IList<Talk>, IList<TalkDTO>>(result);
        }

        public void Remove(int id)
        {
            var result = _db.Talks.GetAll().First(x => x.Id == id);
            if(result == null)
                throw new ValidationException("Item not found",$"{nameof(id)}");
            _db.Talks.Delete(id);
        }

        public void Update(TalkDTO item)
        {
            var temp1 = _db.Talks.GetAll().First(tlk => tlk.Id == item.Id);
            var temp2 = _mapper.Map<TalkDTO, Talk>(item);

            var changed = FieldUpdater(temp2, temp1);

            _db.Talks.Update(changed);
            _db.Save();
        }

        private Talk FieldUpdater(Talk changes, Talk changed)
        {
            changed.FromWho = changes.FromWho;
            changed.ToWho = changes.ToWho;
            changed.Messages = changes.Messages;
            changed.HasNew = changes.HasNew;

            return changed;
        }
    }
}