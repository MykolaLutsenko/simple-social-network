﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SimpleSocialNetwork.Bll.DTO;
using SimpleSocialNetwork.Bll.Infrastructure;
using SimpleSocialNetwork.Bll.Interfaces;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Bll.Services
{
    public class UserProfileService : IUserProfileService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;

        public UserProfileService(IUnitOfWork uow)
        {
            this._db = uow;
            _mapper = AutoMapperConfigBll.Mapper;
        }

        public void Add(UserProfileDTO item)
        {
            if (item == null)
                throw new ValidationException("Item can not be null",$"{nameof(item)}");
            _db.UserProfiles.Create(_mapper.Map<UserProfileDTO,UserProfile>(item));
        }

        public UserProfileDTO Get(int id)
        {
            var result = _db.UserProfiles.GetAll().Where(x => x.Id == id);
            if (result == null)
                throw new NullReferenceException($"UserProfile with this id: {id} not found.");
            return _mapper.Map<UserProfile,UserProfileDTO>(result.First());
        }

        public IList<UserProfileDTO> GetAll()
        {
            return _mapper.Map<IList<UserProfile>, IList<UserProfileDTO>>(_db.UserProfiles.GetAll().ToList());
        }

        public void Remove(int id)
        {
            var result = _db.UserProfiles.Find(user => user.Id == id);
            if(result == null)
                throw new NullReferenceException($"UserProfile with this id: {id} not found.");
            _db.UserProfiles.Delete(id);
        }
    }
}