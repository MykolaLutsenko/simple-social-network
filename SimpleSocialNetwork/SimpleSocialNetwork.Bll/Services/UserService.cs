﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SimpleSocialNetwork.Bll.DTO;
using SimpleSocialNetwork.Bll.Infrastructure;
using SimpleSocialNetwork.Bll.Interfaces;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Bll.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _db;

        public UserService(IUnitOfWork uow)
        {
            _db = uow;
            _mapper = AutoMapperConfigBll.Mapper;
        }

        public void Add(UserDTO item)
        {
            if (item == null)
                throw new ValidationException("Item can not be null",$"{nameof(item)}");
            _db.Users.Create(_mapper.Map<UserDTO,User>(item));
        }

        public UserDTO Get(int id)
        {
            var result = _db.Users.GetAll().First(x => x.Id == id);
            if (result == null)
                throw new NullReferenceException($"User with this id: {id} not ound.");
            return _mapper.Map<User,UserDTO>(result);
        }

        public IList<UserDTO> GetAll()
        {
            var x = _mapper.Map<IList<User>, IList<UserDTO>>(_db.Users.GetAll().ToList());
            return x;
        }

        public void Remove(int id)
        {
            var result = _db.Users.Find(user => user.Id == id);
            if(result == null)
                throw new NullReferenceException($"User with this id: {id} not found.");
            _db.Users.Delete(id);
        }

        public IList<UserDTO> GetAllWithDetails()
        {
            var x = _mapper.Map<IList<User>,IList<UserDTO>>(_db.Users.ReturnAllWithDetails());
            return x;
        }

        public void Update(UserDTO item)
        {
            var temp1 = _db.Users.Find(usr => usr.Id == item.Id).First();

            var temp2 = _mapper.Map<UserDTO, User>(item);

            var changed = FieldUpdater(temp2, temp1);

            _db.Users.Update(changed);
            _db.Save();
        }

        private User FieldUpdater(User changes, User changed)
        {
            changed.FirstName = changes.FirstName;
            changed.LastName = changes.LastName;
            changed.Email = changes.Email;
            changed.UserProfile.Gender = changes.UserProfile.Gender;
            changed.UserProfile.Country = changes.UserProfile.Country;
            changed.UserProfile.Town = changes.UserProfile.Town;

            return changed;
        }

    }
}