﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SimpleSocialNetwork.Bll.DTO;
using SimpleSocialNetwork.Bll.Infrastructure;
using SimpleSocialNetwork.Bll.Interfaces;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Bll.Services
{
    public class MessageService : IMessageService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _db;

        public MessageService(IUnitOfWork uow)
        {
            _db = uow;
            _mapper = AutoMapperConfigBll.Mapper;
        }

        public void Add(MessageDTO item)
        {
            if(item == null)
                throw new ValidationException("Item can not be null",$"{nameof(item)}");
            var temp = _mapper.Map<MessageDTO, Message>(item);

            _db.Messages.Create(temp);
        }

        public MessageDTO Get(int id)
        {
            var result = _db.Messages.GetAll().First(x=>x.Id == id);
            if(result == null)
                throw new ValidationException("Not found item with this id",$"{nameof(id)}");
            return _mapper.Map<Message, MessageDTO>(result);
        }

        public IList<MessageDTO> GetAll()
        {
            var result = _db.Messages.GetAll();
            return _mapper.Map<IList<Message>, IList<MessageDTO>>(result);
        }

        public void Remove(int id)
        {
            var result = _db.Messages.GetAll().First(x => x.Id == id);
            if(result == null)
                throw new ValidationException("Item not found",$"{nameof(id)}");
            _db.Messages.Delete(id);
        }

        public void Update(MessageDTO item)
        {
            var temp1 = _db.Messages.GetAll().First(msg => msg.Id == item.Id);
            var temp2 = _mapper.Map<MessageDTO, Message>(item);

            var changed = FieldUpdater(temp2, temp1);

            _db.Messages.Update(changed);
            _db.Save();
        }

        private Message FieldUpdater(Message changes, Message changed)
        {
            changed.DateTime = changes.DateTime;
            changed.User = changes.User;
            changed.Talk = changes.Talk;
            changed.Text = changes.Text;
            changed.IsRead = changes.IsRead;

            return changed;
        }
    }
}