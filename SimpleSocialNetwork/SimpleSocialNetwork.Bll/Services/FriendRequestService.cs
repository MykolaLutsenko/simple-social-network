﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SimpleSocialNetwork.Bll.DTO;
using SimpleSocialNetwork.Bll.Infrastructure;
using SimpleSocialNetwork.Bll.Interfaces;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Bll.Services
{
    public class FriendRequestService : IFriendRequestService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _db;

        public FriendRequestService(IUnitOfWork uow)
        {
            _db = uow;
            _mapper = AutoMapperConfigBll.Mapper;
        }

        public void Add(int parentId, int childId)
        {
            var result = new FriendRequest()
            {
                ParentId = parentId,
                Parent = _db.Users.Find(x=>x.Id == parentId).First(),
                ChildId = childId,
                Child = _db.Users.Find(x=>x.Id==childId).First(),
                IsItOwnRequest = true

            };
            
            _db.FriendsRequests.Create(result);
        }

        public FriendRequestDTO Get(int id)
        {
            var result = _db.FriendsRequests.GetAll().First(x => x.Id == id);
            if (result == null)
                throw new NullReferenceException($"User with this id: {id} not ound.");
            return _mapper.Map<FriendRequest,FriendRequestDTO>(result);
        }

        public IList<FriendRequestDTO> GetAll()
        {
            return _mapper.Map<IList<FriendRequest>, IList<FriendRequestDTO>>(_db.FriendsRequests.GetAll().ToList());
        }

        public void Remove(int id)
        {
            var result = _db.FriendsRequests.Find(fr => fr.Id == id).First();
            if(result == null)
                throw new NullReferenceException($"FriendRequests with this id: {id} not found.");
            _db.FriendsRequests.Delete(id);
        }

        public IList<FriendRequestDTO> GetAllWithDetails()
        {
            return _mapper.Map<IList<FriendRequest>,IList<FriendRequestDTO>>(_db.FriendsRequests.ReturnAllWithDetails());
        }

        public void Update(FriendRequestDTO item)
        {
            var temp = _db.FriendsRequests.Find(fr => fr.Id == item.Id).First();

            var updateItem = _mapper.Map<FriendRequestDTO, FriendRequest>(item);

            var changed = FieldUpdater(updateItem, temp);

            _db.FriendsRequests.Update(changed);
            _db.Save();
        }

        private FriendRequest FieldUpdater(FriendRequest changes, FriendRequest changed)
        {
            changed.ParentId = changes.ParentId;
            changed.Parent = changes.Parent;
            changed.ChildId = changes.ChildId;
            changed.Child = changes.Child;
            changed.IsItOwnRequest = changes.IsItOwnRequest;
            
            return changed;
        }
    }
}