﻿using AutoMapper;
using SimpleSocialNetwork.Bll.DTO;
using SimpleSocialNetwork.Dal.Entities;

namespace SimpleSocialNetwork.Bll.Infrastructure
{
    public static class AutoMapperConfigBll
    {
        public static IMapper Mapper { get; private set; }

        public static void Init()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDTO>().IncludeMembers(x=>x.Talks).ReverseMap();
                cfg.CreateMap<User, UserDTO>()
                    .ForMember(p => p.Talks, c => c.MapFrom(opt => opt.Talks))
                    .ReverseMap();

                cfg.CreateMap<UserProfile, UserProfileDTO>().ReverseMap();

                cfg.CreateMap<Post, PostDTO>().ReverseMap();

                cfg.CreateMap<Talk, TalkDTO>().IncludeMembers(x=>x.Messages).ReverseMap();
                cfg.CreateMap<Talk, TalkDTO>()
                    .ForMember(p => p.Messages, c => c.MapFrom(opt => opt.Messages))
                    .ReverseMap();

                cfg.CreateMap<Message, MessageDTO>().ReverseMap();

                cfg.CreateMap<FriendRelation, FriendRelationDTO>().ReverseMap();

                cfg.CreateMap<FriendRequest, FriendRequestDTO>().ReverseMap();

            });

            Mapper = config.CreateMapper();
        }
    }
}