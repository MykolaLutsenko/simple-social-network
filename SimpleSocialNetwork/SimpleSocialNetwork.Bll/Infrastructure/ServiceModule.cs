﻿using Ninject.Modules;
using SimpleSocialNetwork.Dal.Interfaces;
using SimpleSocialNetwork.Dal.Repositories;

namespace SimpleSocialNetwork.Bll.Infrastructure
{
    public class ServiceModule : NinjectModule
    {
        private readonly string _connectionString;

        public ServiceModule(string connection)
        {
            _connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitOfWork>().To<EfUnitOfWork>().WithConstructorArgument(_connectionString);
        }
    }
}