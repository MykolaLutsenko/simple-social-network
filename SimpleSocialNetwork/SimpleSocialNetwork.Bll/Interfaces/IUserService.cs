﻿using System.Collections.Generic;
using SimpleSocialNetwork.Bll.DTO;

namespace SimpleSocialNetwork.Bll.Interfaces
{
    public interface IUserService
    {
        void Add(UserDTO item);
        UserDTO Get(int id);
        IList<UserDTO> GetAll();
        void Remove(int id);
        IList<UserDTO> GetAllWithDetails();
        void Update(UserDTO item);
    }
}