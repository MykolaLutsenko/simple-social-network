﻿using System.Collections.Generic;
using SimpleSocialNetwork.Bll.DTO;

namespace SimpleSocialNetwork.Bll.Interfaces
{
    public interface ITalkService
    {
        void Add(TalkDTO item);
        TalkDTO Get(int id);
        IList<TalkDTO> GetAll();
        void Remove(int id);
        void Update(TalkDTO item);
    }
}