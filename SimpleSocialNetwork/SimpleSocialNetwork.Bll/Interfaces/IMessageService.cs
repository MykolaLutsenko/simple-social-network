﻿using System.Collections.Generic;
using SimpleSocialNetwork.Bll.DTO;

namespace SimpleSocialNetwork.Bll.Interfaces
{
    public interface IMessageService
    {
        void Add(MessageDTO item);
        MessageDTO Get(int id);
        IList<MessageDTO> GetAll();
        void Remove(int id);
        void Update(MessageDTO item);
    }
}