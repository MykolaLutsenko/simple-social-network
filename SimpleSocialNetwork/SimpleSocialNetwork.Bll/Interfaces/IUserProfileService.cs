﻿using System.Collections.Generic;
using SimpleSocialNetwork.Bll.DTO;

namespace SimpleSocialNetwork.Bll.Interfaces
{
    public interface IUserProfileService
    {
        void Add(UserProfileDTO item);
        UserProfileDTO Get(int id);
        IList<UserProfileDTO> GetAll();
        void Remove(int id);
    }
}