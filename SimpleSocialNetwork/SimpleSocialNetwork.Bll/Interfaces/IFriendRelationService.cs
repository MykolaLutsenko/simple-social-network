﻿using System.Collections.Generic;
using SimpleSocialNetwork.Bll.DTO;

namespace SimpleSocialNetwork.Bll.Interfaces
{
    public interface IFriendRelationService
    {
        void Add(int parentId, int childId);
        FriendRelationDTO Get(int id);
        IList<FriendRelationDTO> GetAll();
        void Remove(int id);
        IList<FriendRelationDTO> GetAllWithDetails();
        void Update(FriendRelationDTO item);
    }
}