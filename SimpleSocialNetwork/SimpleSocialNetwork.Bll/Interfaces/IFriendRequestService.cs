﻿using System.Collections.Generic;
using SimpleSocialNetwork.Bll.DTO;

namespace SimpleSocialNetwork.Bll.Interfaces
{
    public interface IFriendRequestService
    {
        void Add(int parentId, int childId);
        FriendRequestDTO Get(int id);
        IList<FriendRequestDTO> GetAll();
        void Remove(int id);
        IList<FriendRequestDTO> GetAllWithDetails();
        void Update(FriendRequestDTO item);
    }
}