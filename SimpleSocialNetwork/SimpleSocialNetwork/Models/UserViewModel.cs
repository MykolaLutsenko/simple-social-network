﻿using System.Collections.Generic;

namespace SimpleSocialNetwork.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public virtual UserProfileViewModel UserProfile { get; set; }
        public virtual ICollection<FriendRelationViewModel> Friends { get; set; }
        public virtual ICollection<FriendRequestViewModel> Requests { get; set; }
        public virtual ICollection<PostViewModel> Posts { get; set; }
        public virtual ICollection<TalkViewModel> Talks { get; set; }
        public UserViewModel()
        {
            Requests = new List<FriendRequestViewModel>();
            Friends = new List<FriendRelationViewModel>();
            Posts = new List<PostViewModel>();
            Talks = new List<TalkViewModel>();
        }
    }
}