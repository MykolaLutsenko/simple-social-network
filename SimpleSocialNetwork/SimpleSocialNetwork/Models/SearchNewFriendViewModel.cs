﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SimpleSocialNetwork.Models
{
    public class SearchNewFriendViewModel
    {
        [Required(ErrorMessage = "Name is required." )]
        [RegularExpression(@"^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Surname is required." )]
        [RegularExpression(@"^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$")]
        public string Surname { get; set; }

        public IEnumerable<SelectListItem> Countries { get; set; }

        [Required]
        public string Country { get; set; }
        public IEnumerable<SelectListItem> Cities { get; set; }

        [Required]
        public string City { get; set; }

        public Gender Gender { get; set; }
        public string StringGender { get; set; }
        public IEnumerable<SelectListItem> TemplateGenders { get; set; }
    }
}