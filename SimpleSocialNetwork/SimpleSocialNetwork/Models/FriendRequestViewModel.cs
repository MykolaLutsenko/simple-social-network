﻿namespace SimpleSocialNetwork.Models
{
    public class FriendRequestViewModel
    {
        public int Id { get; set; }

        public bool IsItOwnRequest { get; set; }

        public int? ParentId { get; set; }
        public virtual UserViewModel Parent { get; set; }

        public int? ChildId { get; set; }
        public virtual UserViewModel Child { get; set; }
    }
}