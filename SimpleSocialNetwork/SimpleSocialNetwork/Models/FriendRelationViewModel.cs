﻿namespace SimpleSocialNetwork.Models
{
    public class FriendRelationViewModel
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }
        public virtual UserViewModel Parent { get; set; }

        public int? ChildId { get; set; }
        public virtual UserViewModel Child { get; set; }
    }
}