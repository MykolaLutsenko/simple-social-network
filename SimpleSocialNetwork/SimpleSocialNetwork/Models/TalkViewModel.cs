﻿using System.Collections.Generic;

namespace SimpleSocialNetwork.Models
{
    public class TalkViewModel
    {
        public int Id { get; set; }
        public virtual UserViewModel FromWho { get; set; }
        public virtual UserViewModel ToWho { get; set; }
        public virtual ICollection<MessageViewModel> Messages { get; set; }
        public bool HasNew { get; set; }

        public TalkViewModel()
        {
            Messages = new List<MessageViewModel>();
        }
    }
}