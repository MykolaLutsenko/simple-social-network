﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SimpleSocialNetwork.Models
{
    public class FullUserViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required." )]
        [RegularExpression(@"^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Surname is required.")]
        [RegularExpression(@"^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                           @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                           @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        public Gender Gender { get; set; }
        
        public virtual IEnumerable<SelectListItem> Template { get; set; }

        [Required(ErrorMessage = "Town is required.")]
        public string Town { get; set; }

        [Required(ErrorMessage = "Country is required.")]
        public string Country { get; set; }

        public string UserAvatar { get; set; }

        public bool HasFriendRequest { get; set; } = false;

    }
}