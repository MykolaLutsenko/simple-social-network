﻿using System;

namespace SimpleSocialNetwork.Models
{
    public class PostViewModel
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string PostText { get; set; }
        public int? UserId { get; set; }
        public virtual UserViewModel User { get; set; }
    }
}