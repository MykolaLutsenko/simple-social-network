﻿using System;
using System.Collections.Generic;

namespace SimpleSocialNetwork.Models
{
    public class MessageViewModel
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Text { get; set; }
        public virtual TalkViewModel Talk { get; set; }
        public virtual UserViewModel User { get; set; }
        public bool IsRead { get; set; }
    }
}