﻿using System.Collections.Generic;

namespace SimpleSocialNetwork.Models
{
    public class ShowMessageViewModel
    {
        public virtual UserViewModel FromWho { get; set; }
        public virtual UserViewModel ToWho { get; set; }
        public virtual int TalkID { get; set; }
        public virtual ICollection<MessageViewModel> Messages { get; set; }
        public virtual string Text { get; set; }
    }
}