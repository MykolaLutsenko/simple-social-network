﻿namespace SimpleSocialNetwork.Models
{
    public class UserProfileViewModel
    {
        public int Id { get; set; }

        public Gender Gender { get; set; }

        public string Town { get; set; }

        public string Country { get; set; }

        public string UserAvatar { get; set; }

        public int? UserId { get; set; }

        public virtual UserViewModel User { get; set; }
    }
}