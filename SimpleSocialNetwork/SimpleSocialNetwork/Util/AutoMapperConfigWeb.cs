﻿using AutoMapper;
using SimpleSocialNetwork.Bll.DTO;
using SimpleSocialNetwork.Models;

namespace SimpleSocialNetwork.Util
{
    public class AutoMapperConfigWeb
    {
        public static IMapper Mapper { get; private set; }

        public static void Init()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserViewModel, FullUserViewModel>().ReverseMap();

                cfg.CreateMap<UserViewModel, UserDTO>().IncludeMembers(x => x.Talks).ReverseMap();
                cfg.CreateMap<UserViewModel, UserDTO>()
                    .ForMember(p => p.Talks, c => c.MapFrom(opt => opt.Talks))
                    .ReverseMap();

                cfg.CreateMap<UserProfileViewModel, UserProfileDTO>().ReverseMap();

                cfg.CreateMap<PostViewModel, PostDTO>().ReverseMap();
                
                cfg.CreateMap<TalkViewModel, TalkDTO>().IncludeMembers(x=>x.Messages).ReverseMap();
                cfg.CreateMap<TalkViewModel, TalkDTO>()
                    .ForMember(p => p.Messages, c => c.MapFrom(opt => opt.Messages))
                    .ReverseMap();
                
                cfg.CreateMap<MessageViewModel, MessageDTO>().ReverseMap();
                
                cfg.CreateMap<FriendRelationViewModel,FriendRelationDTO>().ReverseMap();
                
                cfg.CreateMap<FriendRequestViewModel, FriendRequestDTO>().ReverseMap();
                
            });

            Mapper = config.CreateMapper();
        }
    }
}