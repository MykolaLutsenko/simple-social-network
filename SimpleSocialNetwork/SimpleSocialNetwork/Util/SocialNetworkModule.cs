﻿using Ninject.Modules;
using SimpleSocialNetwork.Bll.Interfaces;
using SimpleSocialNetwork.Bll.Services;

namespace SimpleSocialNetwork.Util
{
    public class SocialNetworkModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserService>();
            Bind<IUserProfileService>().To<UserProfileService>();
            Bind<IFriendRelationService>().To<FriendRelationService>();
            Bind<IFriendRequestService>().To<FriendRequestService>();
            Bind<ITalkService>().To<TalkService>();
            Bind<IMessageService>().To<MessageService>();
        }
    }
}