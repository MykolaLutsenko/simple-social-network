﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using SimpleSocialNetwork.Bll.DTO;
using SimpleSocialNetwork.Bll.Interfaces;
using SimpleSocialNetwork.Models;
using SimpleSocialNetwork.Util;

namespace SimpleSocialNetwork.Controllers
{
    public class MessagingController : Controller
    {
        private readonly IUserService _userService;
        private readonly ITalkService _talkService;
        private readonly IMessageService _messageService;
        private readonly IMapper _mapper;

        public MessagingController(IUserService userService, ITalkService talkService, IMessageService messageService)
        {
            _userService = userService;
            _talkService = talkService;
            _messageService = messageService;
            _mapper = AutoMapperConfigWeb.Mapper;
        }

        // GET: Messaging
        [HttpGet]
        public ActionResult Talk()
        {
            //todo:rewright for using identity
            var user = _userService.GetAll().First(x => x.Id == 1);
            //var talk = _talkService.GetAll().First(x => x.Id == 1);
            //user.Talks = new List<TalkDTO>(){talk};
            return View(_mapper.Map<UserDTO,UserViewModel>(user));
        }

        public ActionResult ShowMessage(int id)
        {
            var user = _userService.GetAll().First(x => x.Id == 1);
            var userVM = _mapper.Map<UserDTO, UserViewModel>(user);

            var messageView = new ShowMessageViewModel()
            {
                FromWho = userVM.Talks.First(x=>x.Id == id).FromWho,
                ToWho = userVM.Talks.First(x=>x.Id == id).ToWho,
                Messages = userVM.Talks.First(x=>x.Id == id).Messages,
                TalkID = id
            };
            return PartialView(messageView);
        }

        [HttpPost]
        public ActionResult AddNewMessage(ShowMessageViewModel smvm)
        {
            //todo:rewright for using identity
            var user = _userService.Get(1);

            var talk = _talkService.GetAll().First(x => x.Id == smvm.TalkID);

            var changes = new MessageDTO()
            {
                Id = talk.Messages.Count+1,
                User = user,
                Text = smvm.Text,
                IsRead = false,
                Talk = talk,
                DateTime = DateTime.Now
            };
            
            _messageService.Add(changes);

            return RedirectToAction("ShowMessage",new RouteValueDictionary(new {id = talk.Id}));
        }
    }
}