﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.Ajax.Utilities;
using SimpleSocialNetwork.Bll.DTO;
using SimpleSocialNetwork.Bll.Interfaces;
using SimpleSocialNetwork.Models;
using SimpleSocialNetwork.Util;
using Gender = SimpleSocialNetwork.Models.Gender;

namespace SimpleSocialNetwork.Controllers
{
    public class FriendsController : Controller
    {
        private readonly IUserService _userService;
        private readonly IUserProfileService _userProfileService;
        private readonly IFriendRelationService _friendRelationService;
        private readonly IFriendRequestService _friendRequestService;
        private readonly ITalkService _talkService;
        private readonly IMessageService _messageService;
        private readonly IMapper _mapper;

        public FriendsController(IUserService service, 
                                 IUserProfileService userProfileService,
                                 IFriendRelationService friendRelationService,
                                 IFriendRequestService friendRequestService,
                                 ITalkService talkService,
                                 IMessageService messageService
                                 )
        {
            _userService = service;
            _userProfileService = userProfileService;
            _friendRelationService = friendRelationService;
            _friendRequestService = friendRequestService;
            _talkService = talkService;
            _messageService = messageService;
            _mapper = AutoMapperConfigWeb.Mapper;
        }

        // GET: UserProfile
        public ActionResult Index()
        {
            //todo:rewright for using identity
            var user = _mapper.Map<UserDTO,UserViewModel>(_userService.GetAllWithDetails().First(u => u.Id == 1));

            return View(user);
        }

        public ActionResult FriendsInvites()
        {
            //todo:rewright for using identity
            var user = _mapper.Map<UserDTO,UserViewModel>(_userService.GetAllWithDetails().First(u => u.Id == 1));

            return View(user);
        }

        public ActionResult Unfriend(int id)
        {

            _friendRelationService.Remove(id);
            
            return RedirectToAction("Index");
        }

        public ActionResult AcceptFriendRequest(FriendRequestViewModel friendRequest)
        {
            if (!friendRequest.IsItOwnRequest)
            {
                //me
                _friendRelationService.Add((int)friendRequest.ParentId, (int)friendRequest.ChildId);
                //my friend
                _friendRelationService.Add((int)friendRequest.ChildId,(int)friendRequest.ParentId);

                _friendRequestService.Remove(friendRequest.Id);
            }

            var result = _userService.GetAllWithDetails().First(x => x.Id == 1);

            return View("FriendsInvites",_mapper.Map<UserDTO, UserViewModel>(result));
        }

        public ActionResult DiscardFriendRequest(FriendRequestViewModel friendRequest)
        {
            _friendRequestService.Remove(friendRequest.Id);

            var result = _userService.GetAllWithDetails().First(x => x.Id == 1);

            return View("FriendsInvites",_mapper.Map<UserDTO, UserViewModel>(result));
        }

        [HttpGet]
        public ActionResult FindFriends()
        {
            var data = _userService.GetAllWithDetails();

            var forSearch = new SearchNewFriendViewModel()
             {
                 Countries = data.Select(x => x.UserProfile.Country).AsEnumerable().Where(item=> item != null).Distinct().Select(i=>new SelectListItem()
                 {
                     Text = i?.ToString(),
                     Value = i
                 }).Append(new SelectListItem()
                 {
                     Text = "All",
                     Value = "All",
                     Selected = true
                 }),
                 Cities = data.Select(x => x.UserProfile.Town).AsEnumerable().Where(item=> item != null).Distinct().Select(i=> new SelectListItem()
                 {
                     Text = i.ToString(),
                     Value = i
                 }).Append(new SelectListItem()
                 {
                     Text = "All",
                     Value = "All",
                     Selected = true
                 }),
                 TemplateGenders = ReturnAllGenders().Append(new SelectListItem()
                 {
                     Text = "All",
                     Value = "All",
                     Selected = true
                 })
             };
             return View(forSearch);
        }

        public ActionResult SearchResult(SearchNewFriendViewModel snfvm)
        {
            //todo:rewright for using identity
            int UserId = 1;

            IEnumerable<FullUserViewModel> searchResult;

            searchResult = _mapper.Map<ICollection<UserDTO>, ICollection<UserViewModel>>(_userService.GetAllWithDetails())
                .Select(x =>
                    new FullUserViewModel()
                    {
                        Id = x.Id,
                        FirstName = x.FirstName,
                        LastName = x.LastName,
                        Gender = x.UserProfile.Gender,
                        Template = ReturnAllGenders(),
                        Town = x.UserProfile.Town,
                        Country = x.UserProfile.Country,
                        Email = x.Email,
                        UserAvatar = x.UserProfile.UserAvatar
                    }).AsEnumerable();

            if (!searchResult.Any())
                return HttpNotFound();

            IEnumerable<FullUserViewModel> result = searchResult;

            if (snfvm.Name != null)
                result = result.Where(fn => fn.FirstName == snfvm.Name);
            if (snfvm.Surname != null)
                result = result.Where(sn => sn.LastName == snfvm.Surname);
            if (snfvm.StringGender != null && snfvm.StringGender != "All" )
                result = result.Where(g => g.Gender.ToString() == snfvm.StringGender);
            if (snfvm.Country != null && snfvm.Country != "All")
                result = result.Where(c => c.Country == snfvm.Country);
            if (snfvm.City != null && snfvm.City != "All")
                result = result.Where(c => c.Town == snfvm.City);

            var friendsIds =_userService.GetAllWithDetails().First(u => u.Id == 1)
                        .Friends.Select(x=>x.ChildId)
                        .Where(y=>y != null)
                        .Append(UserId);

            var result2 = result.ToList();

            foreach (var friendId in friendsIds)
            {
                for(int i = 0; i < result2.Count(); i++)
                {
                    if (result2[i].Id == (int)friendId)
                    {
                        result2.Remove(result2[i]);
                    }
                }
            }

            foreach (var friendRequestDto in _friendRequestService.GetAllWithDetails())
            {
                for(int i = 0; i < result2.Count; i++)
                {
                    if (friendRequestDto.ChildId == result2[i].Id && friendRequestDto.ParentId == UserId)
                        result2[i].HasFriendRequest = true;
                }
            }

            return PartialView(result2.AsEnumerable());
        }
        
        public ActionResult SendFriendRequest(int id)
        {
            //todo:rewright for using identity
            int UserId = 1;

            _friendRequestService.Add(UserId, id);

            return PartialView();
        }

        public ActionResult SendMessage(int id)
        {
            //todo:rewright for using identity
            var me = _userService.Get(1);

            var test = me.Talks.Where(x => x.FromWho.Id == me.Id && x.ToWho.Id == id);
            if (test.Count() == 0)
            {
                var userFrom = _userService.Get(1);
                var userTo = _userService.Get(id);
                _talkService.Add(new TalkDTO()
                {
                    FromWho = userFrom,
                    ToWho = userTo,
                    HasNew = false,
                    Messages = new List<MessageDTO>()
                });
            }

            return RedirectToAction("Talk","Messaging");
        }

        private static IEnumerable<SelectListItem> ReturnAllGenders()
        {
            var test4 = (Gender[]) Enum.GetValues(typeof(Gender));

            var test5 = test4.AsEnumerable().Select(x => new SelectListItem()
            {
                Text = x.ToString(),
                Value = x.ToString()
            });

            return test5;
        }
    }
}