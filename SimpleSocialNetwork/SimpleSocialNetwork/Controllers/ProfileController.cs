﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSocialNetwork.Bll.DTO;
using SimpleSocialNetwork.Bll.Interfaces;
using SimpleSocialNetwork.Models;
using SimpleSocialNetwork.Util;
using WebGrease.Css.Extensions;
using Gender = SimpleSocialNetwork.Models.Gender;

namespace SimpleSocialNetwork.Controllers
{
    public class ProfileController : Controller
    {
        private readonly IUserService _userService;
        private readonly IUserProfileService _userProfileService;
        private readonly IMapper _mapper;

        public ProfileController(IUserService service, IUserProfileService userProfileService)
        {
            this._userService = service;
            this._userProfileService = userProfileService;
            this._mapper = AutoMapperConfigWeb.Mapper;
        }

        public ActionResult UserProfile()
        {
            var test =_mapper.Map<UserDTO,UserViewModel>( _userService.GetAllWithDetails().First(user => user.Id == 1));
            ViewBag.User = test;
            TempData["User"] = test;
            
            ViewBag.Message = "Your user profile page.";

            return View();
        }

       public ActionResult EditProfile()
        {
            //todo:rewright for using identity
            var help = TempData["User"] as UserViewModel;
            
            var allGenders = (Gender[]) Enum.GetValues(typeof(Gender));

            var selectListGenders = allGenders.AsEnumerable().Select(x => new SelectListItem()
            {
                Text = x.ToString(),
                Value = x.ToString(),
            }).Select(y =>
            {
                if (y.Value == help.UserProfile.Gender.ToString())
                {
                    y.Selected = true;
                }
                else
                {
                    y.Selected = false;
                }

                return y;
            });

            FullUserViewModel test = new FullUserViewModel()
            {
                Id = help.Id,
                FirstName = help.FirstName,
                LastName = help.LastName,
                Gender = help.UserProfile.Gender,
                Template = selectListGenders,
                Email = help.Email,
                Country = help.UserProfile.Country,
                Town = help.UserProfile.Town,
                UserAvatar = help.UserProfile.UserAvatar
            };
            return View(test);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile(FullUserViewModel uvm)
        {
            if(uvm.FirstName == null)
                ModelState.AddModelError("FirstName","Can not be null");
            if(uvm.LastName == null)
                ModelState.AddModelError("LastName","Can not be null");
            if(uvm.Email == null)
                ModelState.AddModelError("Email","Can not be null");
            if(uvm.Town == null)
                ModelState.AddModelError("Town","Can not be null");
            if(uvm.Country == null)
                ModelState.AddModelError("Country","Can not be null");
            
            if (ModelState.IsValid)
            {
                var changedUserDto =_userService.GetAll().First(user => user.Id == uvm.Id);
            
                var test = ChangeUserData(uvm, changedUserDto);
            
                _userService.Update(test);
            }

            return RedirectToAction("UserProfile");
        }


        private UserDTO ChangeUserData(FullUserViewModel changes, UserDTO changed)
        {
            changed.FirstName = changes.FirstName;
            changed.LastName = changes.LastName;
            changed.Email = changes.Email;
            changed.UserProfile.Gender = (Bll.DTO.Gender)changes.Gender;
            changed.UserProfile.Country = changes.Country;
            changed.UserProfile.Town = changes.Town;

            return changed;
        }
    }
}