﻿using System;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Repositories;

namespace SimpleSocialNetwork.Dal.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<User> Users { get; }
        IRepository<UserProfile> UserProfiles { get; }
        IRepository<Message> Messages { get; }
        IRepository<Talk> Talks { get; }
        IRepository<Post> Posts { get; }
        IRepository<FriendRelation> FriendRelations { get; }
        IRepository<FriendRequest> FriendsRequests { get; }
        void Save();
    }
}