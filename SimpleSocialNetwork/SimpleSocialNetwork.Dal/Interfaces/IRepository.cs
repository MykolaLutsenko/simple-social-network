﻿using System;
using System.Collections.Generic;
using System.Linq;
using SimpleSocialNetwork.Dal.Entities;

namespace SimpleSocialNetwork.Dal.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IList<T> GetAll();
        IList<T> ReturnAllWithDetails();
        T Get(int id); 
        IEnumerable<T> Find(Func<T, Boolean> predicate);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}