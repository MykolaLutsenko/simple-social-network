﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using SimpleSocialNetwork.Dal.Entities;

namespace SimpleSocialNetwork.Dal.EF
{
    public class SocialDbInitializer : DropCreateDatabaseAlways<SocialNetworkContext>
    {
        protected override void Seed(SocialNetworkContext context)
        {
            List<User> users = new List<User>
            {
                new User
                {
                    Id = 1,
                    FirstName = "Profile1",
                    LastName = "Profile1",
                    Email = "profile1@ukr.net",
                    Talks = null,
                    Posts = null,
                    UserProfile = new UserProfile
                    {
                        Country = "Ukraine",
                        Gender = Gender.Male,
                        Town = "Kharkiv",
                        UserAvatar = null
                    }
                },
                new User
                {
                    Id = 2,
                    FirstName = "Profile2",
                    LastName = "Profile2",
                    Email = "profile2@ukr.net",
                    Posts = null,
                    UserProfile = new UserProfile
                    {
                        Country = "Ukraine",
                        Gender = Gender.Female,
                        Town = "Bukovel",
                        UserAvatar = null
                    }
                },
                new User
                {
                    Id = 3,
                    FirstName = "Profile3",
                    LastName = "Profile3",
                    Email = "profile3@ukr.net",
                    Posts = null,
                    UserProfile = new UserProfile
                    {
                        Country = "Belarus",
                        Gender = Gender.Other,
                        Town = "Grodno",
                        UserAvatar = null
                    }
                },
                new User
                {
                    Id = 4,
                    FirstName = "Profile4",
                    LastName = "Profile4",
                    Email = "profile4@ukr.net",
                    Posts = null,
                    UserProfile = new UserProfile
                    {
                        Country = "Ukraine",
                        Gender = Gender.Male,
                        Town = "Borispol",
                        UserAvatar = null
                    }
                },
                new User
                {
                    Id = 5,
                    FirstName = "Profile5",
                    LastName = "Profile5",
                    Email = "profile5@ukr.net",
                    Posts = null,
                    UserProfile = new UserProfile
                    {
                        Country = "USA",
                        Gender = Gender.Female,
                        Town = "Los-Angeles",
                        UserAvatar = null
                    }
                },
                new User
                {
                    Id = 6,
                    FirstName = "Profile6",
                    LastName = "Profile6",
                    Email = "profile6@ukr.net",
                    Posts = null,
                    UserProfile = new UserProfile
                    {
                        Country = "Belarus",
                        Gender = Gender.Male,
                        Town = "Minsk",
                        UserAvatar = null
                    }
                },
            };

            users[0].Friends = new List<FriendRelation>(){
                new FriendRelation()
                {
                    Parent = users[0],
                    Child = users[1]
                },
                new FriendRelation()
                {
                    Parent = users[0],
                    Child = users[2]
                },
                new FriendRelation()
                {
                    Parent = users[0],
                    Child = users[3]
                }
            };

            users[0].Requests = new List<FriendRequest>()
            {
                new FriendRequest()
                {
                    Parent = users[0],
                    Child = users[4],
                    IsItOwnRequest = true
                },
                new FriendRequest()
                {
                    Parent = users[0],
                    Child = users[5],
                    IsItOwnRequest = false
                }
            };
            
            var talks = new List<Talk>()
            {
                new Talk()
                {
                    Id = 1,
                    FromWho = users[0],
                    ToWho = users[1],
                    Messages = null,
                    HasNew = false
                },
                new Talk()
                {
                    Id = 2,
                    FromWho = users[0],
                    ToWho = users[2],
                    Messages = null,
                    HasNew = false
                }
            };

            var messages = new List<Message>()
            {
                new Message()
                {
                    Id = 1,
                    User = users[0],
                    Text = "Hello!",
                    DateTime = DateTime.Now,
                    Talk = talks[0],
                    IsRead = true
                },
                new Message()
                {
                    Id = 2,
                    User = users[1],
                    Text = "Greetings!",
                    DateTime = DateTime.Now,
                    Talk = talks[0],
                    IsRead = true
                },
                new Message()
                {
                    Id = 3,
                    User = users[0],
                    Text = "How are you?!",
                    DateTime = DateTime.Now,
                    Talk = talks[0],
                    IsRead = false
                }
            };
            var messages2 = new List<Message>()
            {
                new Message()
                {
                    Id = 4,
                    User = users[0],
                    Text = "2Hello2!",
                    DateTime = DateTime.Now,
                    Talk = talks[1],
                    IsRead = true
                },
                new Message()
                {
                    Id = 5,
                    User = users[1],
                    Text = "2Greetings2!",
                    DateTime = DateTime.Now,
                    Talk = talks[1],
                    IsRead = true
                },
                new Message()
                {
                    Id = 6,
                    User = users[0],
                    Text = "2How are you2?!",
                    DateTime = DateTime.Now,
                    Talk = talks[1],
                    IsRead = false
                }
            };

            talks[0].Messages = messages;
            talks[0].HasNew = true;

            talks[1].Messages = messages2;
            talks[1].HasNew = true;

            users[0].Talks = talks;

            context.Users.AddRange(users);
            context.SaveChanges();

            
        }
    }
}