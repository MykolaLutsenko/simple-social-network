﻿using System.Data.Entity;
using SimpleSocialNetwork.Dal.Entities;

namespace SimpleSocialNetwork.Dal.EF
{
    public class SocialNetworkContext : DbContext
    {
        static SocialNetworkContext()
        {
            Database.SetInitializer<SocialNetworkContext>(new SocialDbInitializer());
        }

        public SocialNetworkContext()
        {
            
        }

        public SocialNetworkContext(string connectionString) : base(connectionString)
        {
            
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<FriendRelation> Friends { get; set; }
        public virtual DbSet<FriendRequest> FriendsRequests { get; set; }
        public virtual DbSet<Talk> Talks { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        
        public new int SaveChanges()
        {
           return base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOptional(x => x.UserProfile)
                .WithRequired(y => y.User);

            modelBuilder.Entity<User>()
                .HasMany(x=>x.Talks)
                .WithOptional(x=>x.FromWho)
                .HasForeignKey(x=>x.FromWhoId)
                .WillCascadeOnDelete(false);
            
            modelBuilder.Entity<FriendRelation>()
                .HasKey(x=>x.Id)
                .HasRequired( x=>x.Parent)
                .WithMany(x=>x.Friends)
                .HasForeignKey(x=>x.ParentId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<FriendRequest>()
                .HasKey(x=>x.Id)
                .HasRequired(x=>x.Parent)
                .WithMany(x=>x.Requests)
                .HasForeignKey(x=>x.ParentId)
                .WillCascadeOnDelete(true);
        }
    }
}