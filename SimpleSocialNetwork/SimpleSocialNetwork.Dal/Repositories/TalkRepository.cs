﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SimpleSocialNetwork.Dal.EF;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Dal.Repositories
{
    public class TalkRepository : IRepository<Talk>
    {
        private readonly SocialNetworkContext _db;

        public TalkRepository(SocialNetworkContext context)
        {
            this._db = context;
        }

        public IList<Talk> GetAll()
        {
            return _db.Talks.ToList();
        }

        public IList<Talk> ReturnAllWithDetails()
        {
            return _db.Talks.Include(x=>x.Messages).ToList();

        }

        public Talk Get(int id)
        {
            return _db.Talks.Find(id);
        }

        public IEnumerable<Talk> Find(Func<Talk, bool> predicate)
        {
            return _db.Talks.Where(predicate).ToList();
        }

        public void Create(Talk item)
        {
            var newTalk = new Talk()
            {
                FromWho = _db.Users.Find(item.FromWho.Id),
                ToWho = _db.Users.Find(item.ToWho.Id),
                Messages = item.Messages,
                HasNew = true
            };

            _db.Talks.Add(newTalk);
            _db.SaveChanges();
        }

        public void Update(Talk item)
        {
            _db.Entry(item).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            Talk item = _db.Talks.Find(id);

            if (item != null)
                _db.Talks.Remove(item);
        }
    }
}