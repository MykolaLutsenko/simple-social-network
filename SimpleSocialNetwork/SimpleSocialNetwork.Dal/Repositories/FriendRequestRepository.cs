﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SimpleSocialNetwork.Dal.EF;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Dal.Repositories
{
    public class FriendRequestRepository : IRepository<FriendRequest>
    {
        private readonly SocialNetworkContext _db;

        public FriendRequestRepository(SocialNetworkContext context)
        {
            _db = context;
        }

        public IList<FriendRequest> GetAll()
        {
            return _db.FriendsRequests.ToList();
        }

        public IList<FriendRequest> ReturnAllWithDetails()
        {
            return _db.FriendsRequests.Include(p => p.Parent).Include(c => c.Child).ToList();
        }

        public FriendRequest Get(int id)
        {
            return _db.FriendsRequests.ToList().Find(x => x.ParentId == id);
        }

        public IEnumerable<FriendRequest> Find(Func<FriendRequest, bool> predicate)
        {
            return _db.FriendsRequests.Where(predicate).ToList();
        }

        public void Create(FriendRequest item)
        {
            _db.FriendsRequests.Add(item);
            _db.SaveChanges();
        }

        public void Update(FriendRequest item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            FriendRequest item = _db.FriendsRequests.Include(u=>u.Parent)
                .Include(us=>us.Child)
                .ToList().Find(x=>x.Id == id);

            if (item != null)
                _db.FriendsRequests.Remove(item);

            _db.SaveChanges();
        }
    }
}