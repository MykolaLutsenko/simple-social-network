﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SimpleSocialNetwork.Dal.EF;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Dal.Repositories
{
    public class PostRepository : IRepository<Post>
    {
        private readonly SocialNetworkContext _db;

        public PostRepository(SocialNetworkContext context)
        {
            this._db = context;
        }

        public IList<Post> GetAll()
        {
            return _db.Posts.ToList();
        }

        public Post Get(int id)
        {
            return _db.Posts.Find(id);
        }

        public IEnumerable<Post> Find(Func<Post, bool> predicate)
        {
            return _db.Posts.Where(predicate).ToList();
        }

        public void Create(Post item)
        {
            _db.Posts.Add(item);
        }

        public void Update(Post item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var item = _db.Posts.Find(id);
            if (item != null)
                _db.Posts.Remove(item);
        }

        public IList<Post> ReturnAllWithDetails()
        {
            return _db.Posts.ToList();
        }
    }
}