﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SimpleSocialNetwork.Dal.EF;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Dal.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly SocialNetworkContext _db;

        public UserRepository(SocialNetworkContext context)
        {
            this._db = context;
        }
        public IList<User> GetAll()
        {
            var x = _db.Users.ToList();
            return x;
        }

        public User Get(int id)
        {
            return _db.Users.ToList().Find(x=>x.Id == id);
        }

        public IEnumerable<User> Find(Func<User, bool> predicate)
        {
            return _db.Users.Where(predicate).ToList();
        }

        public void Create(User item)
        {
            _db.Users.Add(item);
        }

        public void Update(User item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            User user = _db.Users.Find(id);
            if (user != null)
                _db.Users.Remove(user);
        }

        public IList<User> ReturnAllWithDetails()
        {
            return _db.Users.Include(u => u.UserProfile)
                            .Include(s => s.Friends)
                            .Include(q => q.Requests)
                            .Include(z => z.Talks)
                            .ToList();
        }
    }
}