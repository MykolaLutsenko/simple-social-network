﻿using System;
using SimpleSocialNetwork.Dal.EF;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Dal.Repositories
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private readonly SocialNetworkContext _db;
        private UserRepository _userRepository;
        private UserProfileRepository _userProfileRepository;
        private TalkRepository _talkRepository;
        private MessageRepository _messageRepository;
        private PostRepository _postRepository;
        private FriendRelationRepository _friendRelationRepository;
        private FriendRequestRepository _friendRequestRepository;

        public EfUnitOfWork(string connectionString)
        {
            _db = new SocialNetworkContext(connectionString);
        }

        public IRepository<User> Users => _userRepository ?? (_userRepository = new UserRepository(_db));

        public IRepository<UserProfile> UserProfiles =>
            _userProfileRepository ?? (_userProfileRepository = new UserProfileRepository(_db));

        public IRepository<Talk> Talks => _talkRepository ?? (_talkRepository = new TalkRepository(_db));

        public IRepository<Message> Messages => _messageRepository ?? (_messageRepository = new MessageRepository(_db));

        public IRepository<Post> Posts => _postRepository ?? (_postRepository = new PostRepository(_db));
        public IRepository<FriendRelation> FriendRelations =>
            _friendRelationRepository ?? (_friendRelationRepository = new FriendRelationRepository(_db));

        public IRepository<FriendRequest> FriendsRequests =>
            _friendRequestRepository ??
            (_friendRequestRepository = new FriendRequestRepository(_db));

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this._disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}