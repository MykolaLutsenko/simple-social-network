﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SimpleSocialNetwork.Dal.EF;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Dal.Repositories
{
    public class FriendRelationRepository : IRepository<FriendRelation>
    {
        private readonly SocialNetworkContext _db;

        public FriendRelationRepository(SocialNetworkContext context)
        {
            _db = context;
        }

        public IList<FriendRelation> GetAll()
        {
            return _db.Friends.ToList();
        }

        public IList<FriendRelation> ReturnAllWithDetails()
        {
            return _db.Friends.Include(p => p.Parent).Include(c => c.Child).ToList();
        }

        public FriendRelation Get(int id)
        {
            return _db.Friends.ToList().Find(x => x.ParentId == id);
        }

        public IEnumerable<FriendRelation> Find(Func<FriendRelation, bool> predicate)
        {
            return _db.Friends.Where(predicate).ToList();
        }

        public void Create(FriendRelation item)
        {
            _db.Friends.Add(item);
            _db.SaveChanges();
        }

        public void Update(FriendRelation item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            FriendRelation item = _db.Friends.Include(u=>u.Parent)
                                             .Include(us=>us.Child)
                                             .ToList().Find(x=>x.Id == id);

            if (item != null)
                _db.Friends.Remove(item);

        }
    }
}