﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SimpleSocialNetwork.Dal.EF;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Dal.Repositories
{
    public class MessageRepository : IRepository<Message>
    {
        private readonly SocialNetworkContext _db;

        public MessageRepository(SocialNetworkContext context)
        {
            this._db = context;
        }

        public IList<Message> GetAll()
        {
            return _db.Messages.ToList();
        }

        public Message Get(int id)
        {
            return _db.Messages.Find(id);
        }

        public IEnumerable<Message> Find(Func<Message, bool> predicate)
        {
            return _db.Messages.Where(predicate).ToList();
        }

        public void Create(Message item)
        {
            var test = new Message()
            {
                Id = item.Id,
                User = _db.Users.Find(item.User.Id),
                Text = item.Text,
                Talk = _db.Talks.Find(item.Talk.Id),
                IsRead = item.IsRead,
                DateTime = item.DateTime
            };

            _db.Messages.Add(test);
            _db.SaveChanges();
        }

        public void Update(Message item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var item = _db.Messages.Find(id);
            if (item != null)
                _db.Messages.Remove(item);
        }

        public IList<Message> ReturnAllWithDetails()
        {
            return _db.Messages.ToList();
        }
    }
}