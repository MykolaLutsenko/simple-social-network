﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SimpleSocialNetwork.Dal.EF;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;

namespace SimpleSocialNetwork.Dal.Repositories
{
    public class UserProfileRepository : IRepository<UserProfile>
    {
        private readonly SocialNetworkContext _db;

        public UserProfileRepository(SocialNetworkContext context)
        {
            this._db = context;
        }

        public IList<UserProfile> GetAll()
        {
            return _db.UserProfiles.ToList();
        }

        public UserProfile Get(int id)
        {
            return _db.UserProfiles.Find(id);
        }

        public IEnumerable<UserProfile> Find(Func<UserProfile, bool> predicate)
        {
            return _db.UserProfiles.Where(predicate).ToList();
        }

        public void Create(UserProfile item)
        {
            _db.UserProfiles.Add(item);
        }

        public void Update(UserProfile item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            UserProfile item = _db.UserProfiles.Find(id);
            if (item != null)
                _db.UserProfiles.Remove(item);
        }

        public IList<UserProfile> ReturnAllWithDetails()
        {
            return _db.UserProfiles.Include(up => up.User).ToList();
        }
    }
}