﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleSocialNetwork.Dal.Entities
{
    public class FriendRelation
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int? ParentId { get; set; }
        public virtual User Parent { get; set; }

        public int? ChildId { get; set; }
        public virtual User Child { get; set; }
    }
}