﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SimpleSocialNetwork.Dal.Entities
{
    public class Message
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime DateTime { get; set; }
        [Required]
        public string Text { get; set; }
        [Required]
        public virtual Talk Talk { get; set; }
        [Required]
        public virtual User User { get; set; }
        [Required]
        public bool IsRead { get; set; }
    }
}