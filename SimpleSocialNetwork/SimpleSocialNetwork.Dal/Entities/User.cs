﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SimpleSocialNetwork.Dal.Entities
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }
        
        [Required]
        public virtual UserProfile UserProfile { get; set; }
        
        public virtual ICollection<FriendRelation> Friends { get; set; }

        public virtual ICollection<FriendRequest> Requests { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

        public virtual ICollection<Talk> Talks { get; set; }

        public User()
        {
            Requests = new List<FriendRequest>();
            Friends = new List<FriendRelation>();
            Posts = new List<Post>();
            Talks = new List<Talk>();
        }
    }
}