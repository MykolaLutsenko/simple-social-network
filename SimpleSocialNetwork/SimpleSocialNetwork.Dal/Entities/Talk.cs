﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleSocialNetwork.Dal.Entities
{
    public class Talk
    {
        [Key]
        public int Id { get; set; }

        public int? FromWhoId { get; set; }
        public virtual User FromWho { get; set; }

        public int? ToWhoId { get; set; }
        public virtual User ToWho { get; set; }
        
        public virtual ICollection<Message> Messages { get; set; }
        
        public bool HasNew { get; set; }

        public Talk()
        {
            Messages = new List<Message>();
        }
    }
}