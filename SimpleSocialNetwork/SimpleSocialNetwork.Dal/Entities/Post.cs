﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace SimpleSocialNetwork.Dal.Entities
{
    public class Post
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateTime { get; set; }

        [Required]
        public string PostText { get; set; }

        public int? UserId { get; set; }

        [Required]
        public User User { get; set; }
    }
}