﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SimpleSocialNetwork.Dal.Entities
{
    public class UserProfile
    {
        [Key]
        public int Id { get; set; }

        public Gender Gender { get; set; }

        public string Town { get; set; }

        public string Country { get; set; }

        public string UserAvatar { get; set; }

        public int? UserId { get; set; }
        public virtual User User { get; set; }
    }
}