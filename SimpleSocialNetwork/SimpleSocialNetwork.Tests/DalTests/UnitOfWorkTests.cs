﻿using NUnit.Framework;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Interfaces;
using SimpleSocialNetwork.Dal.Repositories;

namespace SimpleSocialNetwork.Tests.DalTests
{
    [TestFixture]
    public class UnitOfWorkTests
    {
        private EfUnitOfWork uow;

        [SetUp]
        public void SetUp()
        {
            uow = new EfUnitOfWork("Connection");
        }

        [Test]
        public void Users_ReturnSameUsersRepository()
        {
            var x = uow.Users;
            Assert.Multiple(() =>
            {
                Assert.That(x, Is.TypeOf<UserRepository>());
                var y = uow.Users;
                Assert.That(y, Is.EqualTo(x));
            });
        }

        [Test]
        public void UsersProfile_ReturnSameUsersProfileRepository()
        {
            var x = uow.UserProfiles;
            Assert.Multiple(() =>
            {
                Assert.That(x, Is.TypeOf<UserProfileRepository>());
                var y = uow.UserProfiles;
                Assert.That(y, Is.EqualTo(x));
            });
        }

        [Test]
        public void Messages_ReturnSameMessageRepository()
        {
            var x = uow.Messages;
            Assert.Multiple(() =>
            {
                Assert.That(x, Is.TypeOf<MessageRepository>());
                var y = uow.Messages;
                Assert.That(y, Is.EqualTo(x));
            });
        }

        [Test]
        public void Posts_ReturnSameUsers()
        {
            var x = uow.Posts;
            Assert.Multiple(() =>
            {
                Assert.That(x, Is.TypeOf<PostRepository>());
                var y = uow.Posts;
                Assert.That(y, Is.EqualTo(x));
            });
        }
    }
}