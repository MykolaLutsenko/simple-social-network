﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Moq;
using NUnit.Framework;
using SimpleSocialNetwork.Dal.EF;
using SimpleSocialNetwork.Dal.Entities;
using SimpleSocialNetwork.Dal.Repositories;
using SimpleSocialNetwork.Tests.DalTests.Infrastructure;

namespace SimpleSocialNetwork.Tests.DalTests
{
    [TestFixture]
    public class UserRepositoryTests
    {
        private UserRepository userRepository;

        [SetUp]
        public void SetUp()
        {
            var users = new List<User>
            {
                new User {Email = "user1@gmail.com", FirstName = "User1", LastName = "User1", Id = 1},
                new User {Email = "user2@gmail.com", FirstName = "User2", LastName = "User2", Id = 2},
                new User {Email = "user3@gmail.com", FirstName = "User3", LastName = "User3", Id = 3}
            }.AsQueryable();

            var mockSet = new Mock<DbSet<User>>();
            mockSet.As<IQueryable<User>>().Setup(m => m.Provider).Returns(users.Provider);
            mockSet.As<IQueryable<User>>().Setup(m => m.Expression).Returns(users.Expression);
            mockSet.As<IQueryable<User>>().Setup(m => m.ElementType).Returns(users.ElementType);
            mockSet.As<IQueryable<User>>().Setup(m => m.GetEnumerator()).Returns(users.GetEnumerator());
            mockSet.Setup(d => d.Add(It.IsAny<User>())).Callback<User>((s) => users.ToList().Add(s));

            var mockContext = new Mock<SocialNetworkContext>();
            mockContext.Setup(c => c.Users).Returns(mockSet.Object);
            userRepository = new UserRepository(mockContext.Object);
            userRepository.Create(new User());
            var myDbMoq = new Mock<SocialNetworkContext>();
            myDbMoq.Setup(m => m.Users).Returns(SocialNetworkContextMock.GetQueryableMockDbSet(users.ToList()));
            var test = myDbMoq.Object;
            userRepository = new UserRepository(test);
        }

        [TestCase(3)]
        public void GetAll_ReturnAllUsers(int count)
        {

            //arrange
            var x = userRepository.GetAll().Count();
            //act
            
            //assert
            Assert.AreEqual(count, x);
        }

        [Test]
        public void AddNew_ReturnAllUsers()
        {
            //copy repository to make it independent
            var repositoryCopy = userRepository;
            repositoryCopy.Create(new User());
            var x = repositoryCopy.GetAll().ToList().Count;
            Assert.That(x,Is.EqualTo(4));
        }

        [TestCase(1)]
        [TestCase(2)]
        public void GetById_ReturnUser(int id)
        {
            var repositoryCopy = userRepository;
            var sat = repositoryCopy.Get(id);
            Assert.That(sat.Id, Is.EqualTo(id));
        }

        //doesnt work/ dont delete from list
        //[TestCase(1)]
        //public void RemoveUser_ReturnCount(int id)
        //{
        //    var repoCopy = userRepository;
        //    var t2 = repoCopy.GetAll().ToList().Count;
        //    repoCopy.Delete(id);
        //    var t = repoCopy.GetAll().ToList().Count;
        //    Assert.That(repoCopy.GetAll().Count(),Is.EqualTo(2));
        //}

    }
}